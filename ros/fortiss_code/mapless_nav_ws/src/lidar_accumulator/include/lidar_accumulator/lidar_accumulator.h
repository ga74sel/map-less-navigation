#include <ros/ros.h>
#include <string.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <laser_geometry/laser_geometry.h>
#include <pcl_ros/transforms.h>
#include <pcl/common/transforms.h>
#include <pcl/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h> 
#include <sensor_msgs/LaserScan.h>
#include <pcl_ros/point_cloud.h>
#include <Eigen/Dense>
#include "lidar_accumulator/utilities.h"

class LidarAccumulator
{
public:
    LidarAccumulator();
    ~LidarAccumulator();

    void update();

private:
    ros::NodeHandle nh;
    laser_geometry::LaserProjection projector;
    tf::TransformListener tfListener;
    ros::Timer pub_timer;   

    int buffer_size;        // number of scanns to keep in buffer
    float theta_th;         // threshold for inserting new scan
    float d_th;             // threshold for inserting new scan

    bool do_insert_scan;    // true if new scan must be inserted;
    bool is_modified;       // true if buffer has been modified

    ros::Publisher scan_pub;
    ros::Subscriber scan_sub;
    ros::Publisher cloud_pub;

    float angle_min;
    float angle_max;
    float angle_increment;
    float scan_time;
    float range_min;
    float range_max;

    int ds_step;                    // downsample step
    float pub_freq;                 // frequency of acc_scan
    float pub_period;

    std::string robot_name;
    std::string tf_prefix;

    std::string fixed_frame;
    std::string robot_frame;

    vec2d::Pose2d robot_pose, prev_robot_pose;

    std::vector<pcl::PCLPointCloud2> clouds;
    pcl::PCLPointCloud2 acc_cloud;
    sensor_msgs::PointCloud2 acc_pointcloud2;

    size_t cur_idx;
    size_t cnt;

	sensor_msgs::LaserScan acc_scan;    // accumulated scan
    sensor_msgs::LaserScan ds_scan;     // downsampled scan

    /* update robot pose */
    bool update_pose(const std::string& fixed_frame, const std::string& robot_frame, vec2d::Pose2d& pose);

    /* compute robot motion */
    bool motion(const vec2d::Pose2d& pose, vec2d::Pose2d& prev_robot_pose, float theta_th, float d_th);

    /* scan for all laser range finder */
    void scan_callback(const sensor_msgs::LaserScan::ConstPtr& scan);

    void pub_timer_callback(const ros::TimerEvent& timerevent);

    /* convert point cloud to a lidar scan */
    void pointcloud_to_laserscan(Eigen::MatrixXf points, pcl::PCLPointCloud2 *merged_cloud);

    /* downsample lidar scan by some factor */
    void downsample_laserscan(const sensor_msgs::LaserScan::ConstPtr& src, sensor_msgs::LaserScan& dst, int step);

    /* median filter */
    float median(std::vector<float> &vec);
    float mean(std::vector<float>::iterator start, std::vector<float>::iterator end);

    float filter(std::vector<float>::iterator start, std::vector<float>::iterator end, float lower, float upper, float replace);
};
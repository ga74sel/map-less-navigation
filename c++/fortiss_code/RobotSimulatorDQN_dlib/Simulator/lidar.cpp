#include "lidar.h"
#include "simconstants.h"

Lidar::Lidar(int ray_number, float max_range, float visual_angle, float freq, float mean, float std)
    : Sensor(freq, mean, std), ray_number(ray_number), max_range(max_range), visual_angle(visual_angle)
{
    // init ranges
    ranges.resize(ray_number, 0.0f);

    field_points = { b2Vec2(0, 0), b2Vec2(3, 1), b2Vec2(4, 4), b2Vec2(1, 3) };
    field_boarder = { field_points[1]-field_points[0], field_points[2]-field_points[1], field_points[3]-field_points[2], field_points[0]-field_points[3] };

    obstacle = { b2Vec2(2.5, 2.5), b2Vec2(2.8f, 2.6f) };  //b2Vec2(2.8, 2.2)
    obstacle_r = { 0.15f, 0.15f };
}

Lidar::~Lidar()
{
    for(size_t i = 0; i < ray_number; ++i) {
        if( ray_casts[i])
            delete ray_casts[i];
    }
}

void Lidar::attach_body(b2Body* body, b2Vec2 pos, float angle)
{
    // base class
    Sensor::attach_body(body, pos, angle);

    // setup rays
    ray_casts.reserve(ray_number);
    for(size_t i = 0; i < ray_number; ++i) {
        ray_casts.push_back( new RayCast(Sensor::body->GetWorld()) );
    }

    // set points that describe rays in local coordinate frame
    float delta_phi = 2 * visual_angle / (ray_number - 1);
    ray_points.reserve(ray_number);
    float offset = angle - visual_angle;
    for(size_t i = 0; i < ray_number; ++i) {
        ray_points.push_back( max_range * b2Vec2( cosf(i * delta_phi + offset), sinf(i * delta_phi + offset) ));
    }
    start_ang = offset;
    angle_inc = delta_phi;
}

bool Lidar::update(float dt)
{
    if( Sensor::update(dt) ) {
        // time to update sensor
        b2Vec2 start_vec = Sensor::body->GetWorldPoint(pos);
        for( size_t i = 0; i < ray_number; ++i) {
            b2Vec2 end_vec = Sensor::body->GetWorldPoint(ray_points[i]);

            if( ray_casts[i]->cast(start_vec, end_vec, max_range) )
                ranges[i] = ray_casts[i]->get_data().shortestDist + Sensor::real_gaussian(rng); // add noise in distance
            else
                ranges[i] = max_range;
        }
        return true;
    }
    return false;
}

float Lidar::rotation_angle(float src, float dst)
{
    return (src > dst) ? -sim::PI + std::fmod(src - dst + sim::PI, sim::PI_2)
                       :  sim::PI - std::fmod(dst - src + sim::PI, sim::PI_2);
}

void Lidar::update_graphics()
{
    for (size_t i = 0; i < ray_number; ++i) {
        float angle = start_ang + i * angle_inc;
        b2Vec2 p = ranges[i] * b2Vec2( cosf(angle), sinf(angle) );
        ray_casts[i]->update_graphics(Sensor::body->GetWorldPoint(p));
    }
}

void Lidar::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    for (size_t i = 0; i < ray_number; ++i) {
        target.draw(*ray_casts[i]);
    }
}

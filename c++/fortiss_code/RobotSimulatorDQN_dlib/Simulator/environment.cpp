#include "environment.h"
#include "staticobstacle.h"

Environment::Environment(const b2Vec2 &gravity, float loop_frequency, float step_size)
    : loop_frequency(loop_frequency), step_size(step_size), _is_started(false),
      graphic_activated(true), walls_activated(false), grid(b2Vec2(0.5, 0.5))
{    
    // 1. graphics window
    window = new sf::RenderWindow(sf::VideoMode(sim::WINDOWSIZE_X, sim::WINDOWSIZE_Y), "Test");

    // 2. physics simulator
    world = new b2World(gravity);

    // 3. set loop_frequency limit (default: no limit !!!)
    if(loop_frequency > 0)
        window->setFramerateLimit(loop_frequency);
    realtime_factor = loop_frequency * step_size;
}

Environment::~Environment()
{
    // Environment takes full ownership of bodies whithin world
    for(auto iter = bodies.begin(); iter != bodies.end(); ++iter)
        delete *iter;

    delete window;
    delete world; // will free memory for each b2body, fixture object
}

void Environment::add(Body* body, int pos)
{
    on_add(world, body, pos);
}

void Environment::on_add(b2World* world, Body* body, int pos)
{
    // fully constuct body
    body->construct(world);

    // add body to world
    if(pos != -1) {
        pos = std::min(bodies.size() - 1, (size_t)pos);
        bodies.insert(bodies.begin() + pos, body);
    }
    else
        bodies.push_back(body);
}

void Environment::start(bool ok)
{
    _is_started = ok;
}

void Environment::reset(b2AABB* clear_area, Body* body)
{
    if(walls_activated) // do not include walls for resetting (start to count from 4 on)
        on_reset(bodies.begin() + 4, bodies.end(), clear_area, body); // exclude wall form reset
    else
        on_reset(bodies.begin(), bodies.end(), clear_area, body);
}

void Environment::on_reset(std::vector<Body *>::iterator start, std::vector<Body*>::iterator stop, b2AABB* clear_area, Body *reset_body)
{
    // use inital positions
    if(!reset_body)
        for(auto iter = start; iter != stop; ++iter)
            (*iter)->reset();
    else
        reset_body->reset();
}

float Environment::update()
{
    if (window->isOpen() && _is_started)
    {
        // 1. window closed
        sf::Event event;
        while (window->pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                //window->close();
                _is_started = false;
            }
        }

        // 2. measure times/ update frequency
        dt = clock.restart().asSeconds();
        elapsed_time += dt;
        if(cnt % 1000) {
            loop_frequency = 1.0f / elapsed_time;
            realtime_factor = loop_frequency * step_size;
            elapsed_time = 0.0f;
            cnt = 0;
        }

        // 3. simulate physics
        world->Step(step_size, 8, 3);

        // 4. update bodies
        for(auto iter = bodies.begin(); iter != bodies.end(); ++iter)
            (*iter)->update( step_size ); // each body has access to simulaton time

        cnt++;
        return realtime_factor;
    }
    return 0.0f;
}

void Environment::setWall(const b2Vec2& margin)
{
    walls_activated = true;
    StaticRectangle* rect0 = new StaticRectangle(b2Vec2(sim::WORLDSIZE_X / 2.0f, margin.y / 2.0f), b2Vec2(sim::WORLDSIZE_X, margin.y));
    StaticRectangle* rect1 = new StaticRectangle(b2Vec2(sim::WORLDSIZE_X / 2.0f, sim::WORLDSIZE_Y - margin.y / 2.0f), b2Vec2(sim::WORLDSIZE_X, margin.y));
    StaticRectangle* rect2 = new StaticRectangle(b2Vec2(margin.x / 2.0f, sim::WORLDSIZE_Y / 2.0f), b2Vec2(margin.x, sim::WORLDSIZE_Y));
    StaticRectangle* rect3 = new StaticRectangle(b2Vec2(sim::WORLDSIZE_X - margin.x / 2.0f, sim::WORLDSIZE_Y / 2.0f), b2Vec2(margin.x, sim::WORLDSIZE_Y));

    // add to the front (pos = 0) of vector
    Environment::add(rect0, 0);
    Environment::add(rect1, 0);
    Environment::add(rect2, 0);
    Environment::add(rect3, 0);
}

void Environment::update_graphics()
{
    if(graphic_activated)
    {
        for(auto iter = bodies.begin(); iter != bodies.end(); ++iter)
            (*iter)->update_graphics();

        window->clear();
        window->draw(*this);
        window->display();
    }
}

void Environment::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    // backgound
    target.draw(grid, states);

    // bodies
    for(auto iter = bodies.begin(); iter != bodies.end(); ++iter)
        target.draw(**iter);
}

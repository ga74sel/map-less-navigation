#ifndef MAZEENVIRONMENT_H
#define MAZEENVIRONMENT_H

#include "Simulator/environment.h"

class Graph
{
public:
    typedef std::pair<size_t, size_t> Pair;

public:
    Graph(size_t v, size_t e);

    /* add edge between node u and v, with weight w */
    void add_edge(size_t u, size_t v, size_t w);

    /* compute minimum spanning tree nodes */
    std::vector<Pair> kruskal_mst();

private:
    std::vector< std::pair<int, Pair> > edges;
    size_t v, e;
};

// To represent Disjoint Sets
class DisjointSets
{
public:
    // Constructor.
    DisjointSets(size_t n);

    // Find the parent of a node 'u'
    // Path Compression
    size_t find(size_t u);

    // Union by rank
    void merge(size_t x, size_t y);

    std::vector<size_t> parent, rnk;

private:
    size_t n;
};

class MazeEnvironment : public Environment
{
public:
    MazeEnvironment(const b2Vec2& gravity = b2Vec2(0.0f, 0.0f), float loop_frequency = -1.0f, float step_size = 0.05f );
    virtual ~MazeEnvironment();

    /* add new body to environment */
    void on_add(b2World *world, Body *body, int pos = -1);

    /* reset back to inital state
    * clear_area will never be occupied by an object */
    void on_reset(std::vector<Body*>::iterator start, std::vector<Body*>::iterator end, b2AABB* clear_area = nullptr, Body* body = nullptr);

private:
    std::random_device rd;  //  (seed) engine
    std::mt19937 rng;       // random-number engine
    std::uniform_int_distribution<size_t> int_uni;
    std::uniform_real_distribution<float> real_uni, real_uni_x, real_uni_y, real_uni_theta;

    std::vector<Body*> agents;
    size_t rows, cols;
    b2Vec2 tile_size;       // size of maze tiles
    float drop_out;         // 0 = complete maze, 1 = no maze
    int maze_start;         // index of first maze wall

    // randomly sample position for each object in start_obj - end_obj, use maze walls in start_maze - end_maze for collision checks
    void rand_place_bodies(std::vector<Body*>::iterator start_obj, std::vector<Body*>::iterator obj_end,
                           std::vector<Body*>::iterator start_maze, std::vector<Body*>::iterator end_maze,  b2AABB* clear_area = nullptr);

    // compair rectA with each element in start - end
    bool is_colliding(const b2AABB& rectA, std::vector<Body*>::iterator start, std::vector<Body*>::iterator end, const b2Vec2 margin = b2Vec2(0.1f, 0.1f));
};

#endif // MAZEENVIRONMENT_H

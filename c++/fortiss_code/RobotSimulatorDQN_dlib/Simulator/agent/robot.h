#ifndef ROBOT_H
#define ROBOT_H

#include "Simulator/body.h"
#include "Simulator/lidar.h"
#include "Simulator/agent.h"

/*
 * Robot takes part in the evironment physics simulation ( body )
 * and is able to follow commands + percive its environment with sensors ( agent )
 */

class Robot : public Body, public ag::Agent
{
public:
    // position, orientation, radius, standart dev position, standart dev orientation
    Robot(const b2Vec2 &pos, float orientation, float radius, float pos_std = 0.0f, float orient_std = 0.0f, float lin_vel_std = 0.0f, float ang_vel_std = 0.0f );
    virtual ~Robot();

    /* contruct robot object */
    void construct(b2World* world);

    /* retruns the observation the agent has access */
    bool observation( ag::SensorReading& sensorreading );
    /* returns the evnironment state of the agent (pos, velo, orientation, omega)  */
    bool model_states( ag::ModelStates& modelstate );
    /* send a new command to robot */
    void command(const ag::VelCmd &cmd);

    /* reset sensors to inital state */
    void reset(const b2Vec2& pos = b2Vec2(0.0f, 0.0f) , float orientation = 0.0f);
    /* update sensor */
    void update(float dt);
    /* update grafic */
    void update_graphics();

    /* box2d contact callbacks (currently unused) */
    void beginContact(Body* entety, b2Fixture* fixture,  b2Contact* contact) { }
    void endContact(Body* entety, b2Fixture* fixture, b2Contact* contact) { }

    // manipulate box2d body via impulses
    void apply_lin_velo(const b2Vec2& vel);
    void apply_ang_velo(float omega);

private:
    // properites
    float radius;

    // sensors
    Lidar lidar;

    // noise ( addative gaussian noise for robot pose and commands )
    std::random_device rd;                                  // (seed) engine
    std::mt19937 rng;                                       // random-number engine
    std::normal_distribution<float> real_gaussian_pos;      // noise in position
    std::normal_distribution<float> real_gaussian_orient;   // noise in orientation
    std::normal_distribution<float> real_gaussian_lin_vel;  // noise lin velocity
    std::normal_distribution<float> real_gaussian_ang_vel;  // noise angular velocity

    // sfml draw
    void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif // ROBOT_H

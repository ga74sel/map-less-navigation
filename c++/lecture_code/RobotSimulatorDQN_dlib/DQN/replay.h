#ifndef REPLAY_H
#define REPLAY_H

#include "config.h"
#include <dlib/matrix.h>
#include <random>
#include <vector>

namespace dqn {

typedef dlib::matrix<scalar_t> Matrix;
typedef std::vector<Matrix> Batch;

class Memory {
public:
    Batch states;
    std::vector<action_t> actions;
    std::vector<float> rewards;
    Batch next_states;
    std::vector<float> td_errors;
    std::vector<uint8_t> isfinished;

    Memory& operator= ( const Memory& m ) {
        if (this != &m)
        {
            resize( m.get_buffer_size(), m.get_state_size() );
            for (size_t i = 0; i < m.get_buffer_size(); ++i) {
                states[i] = m.states[i];
                actions[i] = m.actions[i];
                rewards[i] = m.rewards[i];
                next_states[i] = m.next_states[i];
                td_errors[i] = m.td_errors[i];
                isfinished[i] = m.isfinished[i];
            }
        }
        return *this;
    }

    void resize(size_t buffer_size, int state_size) {
        states.resize(buffer_size);
        actions.resize(buffer_size);
        rewards.resize(buffer_size);
        next_states.resize(buffer_size);
        td_errors.resize(buffer_size);
        isfinished.resize(buffer_size);

        for(size_t i = 0; i < buffer_size; ++i) {
            states[i].set_size(state_size, 1);
            next_states[i].set_size(state_size, 1);
        }
    }
    size_t get_buffer_size() const { return states.size(); }
    int get_state_size() const { return states[0].nr(); }
};

/* Replay buffer */
class Replay
{
public:
    Replay(size_t buffer_size, size_t batch_size, int state_size = 1);

    /* set new state size */
    void initialize(int state_size);
    void initialize(const Memory& memory, size_t cur_index, bool filled );

    /* buffer completly filled */
    bool is_filled() const { return filled; }

    /* return length of Replay */
    size_t get_buffer_size() const { return buffer_size; }

    /* return state dimension */
    int get_state_size() const { return memory.get_state_size(); }

    size_t get_batch_size() const { return batch_size; }

    /* add new Element to Replay */
    void push_back(const Matrix& state, const size_t& action, const scalar_t& reward, const Matrix& next_state,  const scalar_t& td, bool isfinished);

    /* return references to uniform sampled Replay elements */
    const Memory& sample();

    /* return reference to complete memory */
    const Memory& get_memory() const { return memory; }

    size_t get_current_index() const { return cur_index; }

private:
    bool filled;
    size_t cur_index;

    size_t buffer_size;                        // max buffer size
    size_t batch_size;
    int state_size;

    Memory memory;
    Memory sampled_memory;

    std::random_device rd;                  //  (seed) engine
    std::mt19937 rng;                       // random-number engine
    std::uniform_int_distribution<int> int_uni;
};

}

#endif // REPLAY_H

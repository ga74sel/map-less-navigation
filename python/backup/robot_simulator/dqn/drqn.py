from dqn.networks import DRQNNetwork
import random
import numpy as np
import pickle
import tensorflow as tf

# ----------------------------------------------------------------------------------------------------------------------
# Baseclass for replay buffers
# ----------------------------------------------------------------------------------------------------------------------
class ReplayBase:
    def __init__(self, buffer_size: int, state_size: int):
        # buffer
        self.state_buf = np.zeros((buffer_size, state_size), dtype=np.float32)
        self.action_buf = np.zeros((buffer_size,), dtype=np.int32)
        self.reward_buf = np.zeros((buffer_size,), dtype=np.float32)
        self.state_next_buf = np.zeros((buffer_size, state_size), dtype=np.float32)
        self.done_buf = np.zeros((buffer_size,), dtype=np.int32)
        self.buffer_size = buffer_size
        self._cnt = 0
        self.full = False

    def save(self, file):
        """ save to file """
        out_file = open(file, 'wb')
        pickle.dump((self.state_buf,
                     self.action_buf,
                     self.reward_buf,
                     self.state_next_buf,
                     self.done_buf), out_file)

    def load(self, file):
        """ load form file """
        in_file = open(file, 'rb')
        list_buf = pickle.load(in_file)
        self.state_buf = list_buf[0]
        self.action_buf = list_buf[1]
        self.reward_buf = list_buf[2]
        self.state_next_buf = list_buf[3]
        self.done_buf = list_buf[4]

        self._cnt = len(self.action_buf)-1


# ----------------------------------------------------------------------------------------------------------------------
# Replay buffer that saves transitions
# ----------------------------------------------------------------------------------------------------------------------
class Replay(ReplayBase):
    """ Replay Buffer """
    def __init__(self, buffer_size: int, state_size: int, batch_size: int, sequence_len: int):
        super(Replay, self).__init__(buffer_size, state_size)
        self.batch_size = batch_size
        self.sequence_len = sequence_len

        # batch
        self.state_batch = np.zeros((batch_size * sequence_len, state_size), dtype=np.float32)
        self.action_batch = np.zeros((batch_size * sequence_len,), dtype=np.int32)
        self.reward_batch = np.zeros((batch_size * sequence_len,), dtype=np.float32)
        self.state_next_batch = np.zeros((batch_size * sequence_len, state_size), dtype=np.float32)
        self.done_batch = np.zeros((batch_size * sequence_len,), dtype=np.int32)

    def is_full(self):
        """ true if buffer filled"""
        return self.full

    def add(self, state, action, reward, state_next, done):
        """ add new transition to buffer """
        self.state_buf[self._cnt] = state
        self.action_buf[self._cnt] = action
        self.reward_buf[self._cnt] = reward
        self.state_next_buf[self._cnt] = state_next
        self.done_buf[self._cnt] = done

        self._cnt += 1
        if self._cnt >= self.buffer_size:
            self.full = True
            self._cnt = 0

    def sample(self):
        """
        uniformly sample sequences form reply buffer
        :return: np array of shape (batch_size * sequence_len, x)
        """
        i = 0
        while i < self.batch_size:

            # randomly select index, make sure only complete traces are returned
            while True:
                idx = random.randint(self.sequence_len, self.buffer_size - 1)
                if self.done_buf[(idx - self.sequence_len): idx].any():
                    continue
                break

            seq_range = range(i * self.sequence_len, (i+1) * self.sequence_len)
            seq_idx = range(idx - self.sequence_len + 1, idx + 1)

            self.state_batch[seq_range] = self.state_buf[seq_idx]
            self.action_batch[seq_range] = self.action_buf[seq_idx]
            self.reward_batch[seq_range] = self.reward_buf[seq_idx]
            self.state_next_batch[seq_range] = self.state_next_buf[seq_idx]
            self.done_batch[seq_range] = self.done_buf[seq_idx]
            i += 1

        return self.state_batch, \
               self.action_batch, \
               self.reward_batch, \
               self.state_next_batch, \
               self.done_batch

# ----------------------------------------------------------------------------------------------------------------------
# Double Deep Q-Lerning algorithm
# ----------------------------------------------------------------------------------------------------------------------
class DDRQN:
    def __init__(self, tf_sess,
                 state_size = 20,
                 action_size = 3,
                 buffer_size = 1000000,
                 epsilon = 0.9,
                 alpha = 0.0004,
                 gamma = 0.99,
                 target_update_steps = 5000,
                 n_train_steps = 1,
                 batch_size = 64,
                 sequence_len = 10,
                 ):
        """
        Implementation of Double Qlearning algorithm
        :param state_size: dimension of states
        :param action_size: number of actions
        :param buffer_size: replay buffer size
        :param epsilon: initial epsilon
        :param alpha: learning rate
        :param gamma: discount factor
        :param target_update_steps: steps between target update
        :param n_train_steps: steps between network update ( backporp)
        :param batch_size: number of elements in one batch
        :param sequence_len: number of samples in a sequence to train lstm
        """

        self.tf_sess = tf_sess

        # 1. set hyper parameters
        self.state_size = state_size
        self.action_size = action_size
        self.buffer_size = buffer_size
        self.epsilon = epsilon
        self.alpha = alpha
        self.gamma = gamma
        self.target_update_steps = target_update_steps
        self.n_train_steps = n_train_steps
        self.batch_size = batch_size
        self.sequence_len = sequence_len

        # 1. create networks
        self.model = DRQNNetwork(self.tf_sess, state_size, action_size, self.alpha, "q")
        self.target_model = DRQNNetwork(self.tf_sess, state_size, action_size, self.alpha, "target_q")

        # 3. setup replay buffer
        self.replay = Replay(self.buffer_size, self.state_size, batch_size, sequence_len)
        self.cnt = 0

    def learn(self, state, action, reward, next_state, done):
        """
        Insert new sample into replay buffer, do learning if buffer is completely filled
        """
        self.cnt += 1

        # 2. add sample to replay buffer, use reward as td error during filling phase
        self.replay.add(state, action, reward, next_state, done)

        # 3. once replay buffer is filled, start with experience replay (udpate every n_train_steps)
        if self.replay.is_full():
            if self.cnt % self.n_train_steps == 0:
                return self._experience_replay() # trained
            else:
                return 0.0 # skip
        return -1.0

    def _experience_replay(self):
        """
        get batch form replay buffer, do gradient decent
        """
        # 1. Update target every swap steps
        if self.cnt % self.target_update_steps == 0:
            self._copy_model_parameters(self.model, self.target_model)

        # 2. get samples form replay buffer
        state_batch, action_batch, reward_batch, state_next_batch, done_batch = self.replay.sample()

        # 3. reset rrn internal state before training session
        state_rnn = (np.zeros([self.batch_size, 32]), np.zeros([self.batch_size, 32]))

        # 4. run predictions
        q_values_next = self.model.predict(state_batch, state_rnn, self.batch_size, self.sequence_len)
        q_values_next_t = self.target_model.predict(state_batch, state_rnn, self.batch_size, self.sequence_len)

        # 5. Q(s,a) = r + gamma * argmax_a Q(s', a)
        q_values_next = np.argmax(q_values_next, axis=1)
        end_multiplier = -(done_batch - 1)

        double_q = q_values_next_t[range(self.batch_size * self.sequence_len), q_values_next]
        target_batch = reward_batch + (self.gamma * double_q * end_multiplier)

        # 2. Update model
        return self.model.optimize(state_batch, state_rnn, action_batch, target_batch, self.batch_size, self.sequence_len)

    def generate_action(self, state):
        """
        get eps-greedy actions
        """
        if random.uniform(0, 1) < self.epsilon:
            return random.randint(0, self.action_size - 1)
        else:
            state_rnn = (np.zeros([1, 32]), np.zeros([1, 32]))
            state_np = np.array(state, dtype=np.float32).reshape((1, self.state_size))
            return np.argmax(self.model.predict(state_np, state_rnn, 1, 1))

    def _copy_model_parameters(self, model_1, model_2):
        """
        copy model parameters form model 1 to 2
        """
        e1_params = [t for t in tf.trainable_variables() if t.name.startswith(model_1.scope)]
        e1_params = sorted(e1_params, key=lambda v: v.name)
        e2_params = [t for t in tf.trainable_variables() if t.name.startswith(model_2.scope)]
        e2_params = sorted(e2_params, key=lambda v: v.name)
        update_ops = []
        for e1_v, e2_v in zip(e1_params, e2_params):
            op = e2_v.assign(e1_v)
            update_ops.append(op)

        self.tf_sess.run(update_ops)

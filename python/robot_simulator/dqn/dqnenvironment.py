from simulator.agentenvironment import *
import math
import random
import numpy as np
from itertools import chain


class DQNEnvironment(AgentEnvironement):
    '''
    Basic Environment for DQN Networks
    - converts action labels to agend velocites
    - converts agend sensor readings to state space representation
    - computes the reward based on current state and action
    '''
    def __init__(self, env, agent, goal_position, Config):
        super(DQNEnvironment, self).__iter__(env, agent, Config['num_actions'], Config['num_states'])
        self.goal_position = goal_position

        self.min_goal_dist = Config['min_goal_dist']
        self.min_obstical_dist = Config['min_obstical_dist']
        self.n_rays = Config['n_rays']
        self.max_range = Config['max_range']
        self.use_random_agent_pos = Config['use_random_agent_pos']
        self.use_random_agent_goal_pos = Config['use_random_agent_goal_pos']

        self.rew_dist = Config['rew_dist']
        self.rew_step = Config['rew_step']
        self.rew_potential = Config['rew_potential']

        self.env_state = EnvironmentState()
        self.env_state.state = np.zeros([self.n_state, ])
        self._setup()

    def _setup(self):
        self.clear_goal_area = b2AABB()
        self.clear_goal_area.upperBound = b2Vec2(self.min_goal_dist / 2.0,
                                                 self.min_goal_dist / 2.0) + self.goal_position
        self.clear_goal_area.lowerBound = b2Vec2(-self.min_goal_dist / 2.0,
                                                 -self.min_goal_dist / 2.0) + self.goal_position
        self.prev_action = -1.0
        self.prev_distance = -1.0

    def set_goal_position(self, goal_position):
        self.goal_position = goal_position
        self.clear_goal_area.upperBound = b2Vec2(self.min_goal_dist / 2.0,
                                                 self.min_goal_dist / 2.0) + self.goal_position
        self.clear_goal_area.lowerBound = b2Vec2(-self.min_goal_dist / 2.0,
                                                 -self.min_goal_dist / 2.0) + self.goal_position

    def execute(self, action):
        self.prev_action = action

        vel_cmd = VelCmd()
        vel_cmd.velocity.y = 0.0

        if action == 0:
            vel_cmd.velocity.x = 0.3
            vel_cmd.omega = 0.0
        elif action == 1:
            vel_cmd.velocity.x = 0.04
            vel_cmd.omega = 0.6
        elif action == 2:
            vel_cmd.velocity.x = 0.04
            vel_cmd.omega = -0.6

        self.agent.command( vel_cmd )

    def observe(self):
        # 1. do observation (no new sensor update return none)
        available, sensorreading = self.agent.observe()
        if not available:
            return None
        modelstate = self.agent.model()

        # 2. simulate callbacks
        #reached_goal, distance, theta_cos, theta_sin = self.model_state_callback(modelstate, self.min_goal_dist)
        reached_goal, distance, theta = self.model_state_callback(modelstate, self.min_goal_dist)
        hit_obstacle, normalized_ranges = self.scan_callback(sensorreading, self.min_obstical_dist)

        if self.prev_distance < 0:
            self.prev_distance = distance

        # 3. compute reward
        reward = 0.0
        if hit_obstacle:
            reward = -150.0
        elif reached_goal:
            reward = 100.0
        reward += self.rew_dist * (self.prev_distance - distance)
        reward -= self.rew_step
        reward -= self.rew_potential * self.potential_distance(sensorreading, self.min_obstical_dist, 0.258 )
        reward = min(100.0, max(-150.0, reward))

        # 4. combine into state vector ( normalized to [0, 1] )
        self.env_state.done = reached_goal | hit_obstacle
        self.env_state.success = reached_goal
        self.env_state.reward = reward

        # combine: normalized_ranges, dist to goal, angle to goal
        self.env_state.state[:self.n_rays] = normalized_ranges
        self.env_state.state[-2] = 1.0 / distance * self.min_goal_dist
        self.env_state.state[-1] = theta / sim.PI + 1.0
        #self.env_state.state[-3] = 1.0 / distance * self.min_goal_dist		# cos + sin (complex plane) can be used instead of angle
        #self.env_state.state[-2] = 0.5 * (theta_cos + 1.0)			# changes the dimension! (+1), must be changed in sesssion config!
        #self.env_state.state[-1] = 0.5 * (theta_sin + 1.0)

        # update history
        self.prev_distance = distance

        # 5. done, success, state, reward
        return self.env_state

    def reset(self):
        # 1. reset the environment
        if self.use_random_agent_pos and not self.use_random_agent_goal_pos:
            self.env.reset(self.clear_goal_area)
        elif not self.use_random_agent_pos and self.use_random_agent_goal_pos:
            self.set_goal_position( b2Vec2(random.uniform(0.2, sim.WORLDSIZE_X - 0.2),
                                           random.uniform(0.2, sim.WORLDSIZE_Y - 0.2)))
            if self.env_state.success:
                self.env.reset(self.clear_goal_area, self.agent)
                self.agent.reset_sensors()
            else:
                self.env.reset(self.clear_goal_area)
        else:
            self.set_goal_position( b2Vec2(random.uniform(0.2, sim.WORLDSIZE_X - 0.2),
                                           random.uniform(0.2, sim.WORLDSIZE_Y - 0.2)))
            self.env.reset(self.clear_goal_area)

        # 2. do initial observation
        return self.observe()

    def model_state_callback(self, modelstate, min_range):
        # distance to goal
        distance_vec = self.goal_position - modelstate.position
        distance = math.sqrt(distance_vec.x * distance_vec.x + distance_vec.y * distance_vec.y)
        # angle to goal
        #theta_cos, theta_sin = self.cos_sin_angle(math.atan2(distance_vec.y, distance_vec.x), modelstate.theta)
        theta = self.rotation_angle(math.atan2(distance_vec.y, distance_vec.x), modelstate.theta)

        return distance < min_range, distance, theta #theta_cos, theta_sin

    def scan_callback(self, range_vec, min_range):
        normalized_range_vec = 1.0 / range_vec * min_range
        done = np.any(range_vec < min_range)
        return done, normalized_range_vec

    def rotation_angle(self, src, dst):
        if src > dst:
            return -sim.PI + math.fmod(src - dst + sim.PI, sim.PI_2)
        else:
            return sim.PI - math.fmod(dst - src + sim.PI, sim.PI_2)

    def cos_sin_angle(self, src, dst):
        return math.cos(src - dst), math.sin(src - dst)

    def potential_distance(self, d_vec, d_min, tau):
        return np.mean( np.exp( -(d_vec - d_min) / tau ) )





